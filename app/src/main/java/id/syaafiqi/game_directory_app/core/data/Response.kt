package id.syaafiqi.game_directory_app.core.data

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("message") val message: String = "Something went wrong!"
)