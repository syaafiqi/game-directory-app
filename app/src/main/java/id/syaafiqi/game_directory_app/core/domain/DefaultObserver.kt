package id.syaafiqi.game_directory_app.core.domain

interface DefaultObserver<T : Any> {
    fun onSuccess(entity: T)
    fun onError(exception: Throwable)
}