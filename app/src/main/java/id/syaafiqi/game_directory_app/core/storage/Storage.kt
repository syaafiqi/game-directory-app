package id.syaafiqi.game_directory_app.core.storage

import android.content.Context
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import id.syaafiqi.game_directory_app.BuildConfig
import id.syaafiqi.game_directory_app.core.storage.tables.Favorites

private const val DATABASE_VERSION = 1

private val TABLES: List<StorageTable<*>> = listOf(
    Favorites
)

interface StorageTable<T> {
    fun createTable(db: SQLiteDatabase)
    fun dropTable(db: SQLiteDatabase)
    fun get(db: SQLiteDatabase): List<T>
    fun findOne(db: SQLiteDatabase, id: Int): T?
    fun create(db: SQLiteDatabase, data: T)
    fun update(db: SQLiteDatabase, id: Int, data: T)
    fun delete(db: SQLiteDatabase, id: Int)
}

fun Context.storage(): SQLiteOpenHelper =
    object : SQLiteOpenHelper(this, BuildConfig.APPLICATION_ID, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase?) {
            when {
                db != null -> {
                    TABLES.map {
                        it.createTable(db)
                    }
                }
            }
        }

        override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
            when {
                db != null -> {
                    TABLES.map {
                        it.dropTable(db)
                    }
                }
            }
            onCreate(db)
        }
    }

inline operator fun <reified T : Any> SQLiteOpenHelper.get(table: StorageTable<T>): List<T> =
    table.get(this.writableDatabase)

inline fun <reified T : Any> SQLiteOpenHelper.find(table: StorageTable<T>, id: Int): T? =
    table.findOne(this.writableDatabase, id)

inline fun <reified T : Any> SQLiteOpenHelper.insert(table: StorageTable<T>, data: T) = table.create(this.writableDatabase, data)

inline fun <reified T : Any> SQLiteOpenHelper.update(table: StorageTable<T>, id: Int, data: T) = table.update(this.writableDatabase, id, data)

inline fun <reified T : Any> SQLiteOpenHelper.delete(table: StorageTable<T>, id: Int) = table.delete(this.writableDatabase, id)