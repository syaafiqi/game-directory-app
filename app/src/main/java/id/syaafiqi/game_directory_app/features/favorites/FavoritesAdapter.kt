package id.syaafiqi.game_directory_app.features.favorites

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.game_directory_app.R
import id.syaafiqi.game_directory_app.core.storage.tables.FavoritesModel
import id.syaafiqi.game_directory_app.databinding.ItemGameBinding
import id.syaafiqi.game_directory_app.databinding.ItemLoadingBinding
import id.syaafiqi.game_directory_app.extensions.load
import id.syaafiqi.game_directory_app.extensions.show

class FavoritesAdapter(
    private var gamesList: ArrayList<FavoritesModel>,

    private val listener: ViewAction
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoading = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> LoadMoreViewHolder(
                ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            else -> GamesViewHolder(
                ItemGameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }

    inner class GamesViewHolder(private val view: ItemGameBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: FavoritesModel) {
            with(view) {
                view.ivGameThumbnail.load(data.imageUrl)
                tvGameTitle.text = data.name
                tvGameReleaseDate.text = root.context.getString(R.string.release_date_label, data.releaseDate)
                tvGameRating.text = data.rating.toString()
                view.root.setOnClickListener {
                    listener.itemClick(data)
                }
            }

        }
    }

    inner class LoadMoreViewHolder(private val view: ItemLoadingBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            with(view) {
                loadingStateRv.show()
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when {
        position == gamesList.size && isLoading -> 0
        else -> 1
    }

    override fun getItemCount(): Int = when (isLoading) {
        true -> gamesList.size + 1
        else -> gamesList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoadMoreViewHolder -> holder.bind()
            is GamesViewHolder -> holder.bind(gamesList[position])
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addItems(addedItems: ArrayList<FavoritesModel>, isLastPage: Boolean = false) {
        isLoading = !isLastPage
        gamesList.addAll(addedItems)
        notifyDataSetChanged()
    }

    interface ViewAction {
        fun itemClick(data: FavoritesModel)
    }
}