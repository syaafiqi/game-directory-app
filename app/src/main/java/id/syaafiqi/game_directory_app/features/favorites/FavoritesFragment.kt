package id.syaafiqi.game_directory_app.features.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.game_directory_app.core.storage.get
import id.syaafiqi.game_directory_app.core.storage.storage
import id.syaafiqi.game_directory_app.core.storage.tables.Favorites
import id.syaafiqi.game_directory_app.core.storage.tables.FavoritesModel
import id.syaafiqi.game_directory_app.databinding.FragmentFavoritesBinding
import id.syaafiqi.game_directory_app.extensions.gotoActivity
import id.syaafiqi.game_directory_app.extensions.hide
import id.syaafiqi.game_directory_app.extensions.show
import id.syaafiqi.game_directory_app.features.home.GameModel
import id.syaafiqi.game_directory_app.features.home.HomeAdapter
import id.syaafiqi.game_directory_app.features.view_game.ViewGameActivity
import id.syaafiqi.game_directory_app.utils.Constants

class FavoritesFragment: Fragment() {
    private var _binding: FragmentFavoritesBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var favorites: List<FavoritesModel> = emptyList()
    private lateinit var adapter: FavoritesAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)

        initView()
        setActions()

        return binding.root
    }

    private fun initView() {
        try {
            val fetch = context?.storage()?.get(Favorites)
            fetch?.let {
                when(it.size) {
                    0 -> binding.emptyState.root.show()
                    else -> {
                        favorites = fetch
                        adapter = FavoritesAdapter(ArrayList(favorites), RecyclerViewActionObserver())
                        binding.rvGames.adapter = adapter
                        setLinearRecyclerView()
                    }
                }
            } ?: run { binding.emptyState.root.show() }
            binding.gamesRefresherPanel.isRefreshing = false
        }
        catch (ex: Exception) {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }
    }

    private fun setActions() {
        binding.gamesRefresherPanel.setOnRefreshListener {
            initView()
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                adapter = FavoritesAdapter(ArrayList(favorites.filter { x -> x.name.lowercase().contains(p0?.lowercase() ?: "") }), RecyclerViewActionObserver())
                binding.rvGames.adapter = adapter
                setLinearRecyclerView()
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean = false
        })
        binding.searchView.setOnCloseListener {
            adapter = FavoritesAdapter(ArrayList(favorites), RecyclerViewActionObserver())
            binding.rvGames.adapter = adapter
            setLinearRecyclerView()
            false
        }
    }


    private fun setLinearRecyclerView() {
        binding.rvGames.layoutManager = LinearLayoutManager(binding.root.context)
    }

    inner class RecyclerViewActionObserver : FavoritesAdapter.ViewAction {
        override fun itemClick(data: FavoritesModel) {
            activity?.gotoActivity(ViewGameActivity::class, extras = mapOf(Constants.GAME_INFO to GameModel(
                id = data.id,
                name = data.name,
                imageUrl = data.imageUrl,
                rating = data.rating,
                ratingCount = data.ratingCount,
                tba = data.tba,
                releaseDate = data.releaseDate
            )))
        }
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}