package id.syaafiqi.game_directory_app.features.home

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GameModel(
    val id: Int,
    val name: String,
    val rating: Double,
    val ratingCount: Int,
    val tba: Boolean,
    val releaseDate: String,
    val imageUrl: String
) : Parcelable
