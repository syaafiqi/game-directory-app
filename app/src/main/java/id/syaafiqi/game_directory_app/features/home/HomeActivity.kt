package id.syaafiqi.game_directory_app.features.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.syaafiqi.game_directory_app.R
import id.syaafiqi.game_directory_app.databinding.ActivityHomeBinding
import id.syaafiqi.game_directory_app.extensions.replaceFragment
import id.syaafiqi.game_directory_app.features.favorites.FavoritesFragment

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
    }

    private fun initView() {
        // Default view
        supportActionBar?.title = getString(R.string.games_for_you)
        replaceFragment(HomeFragment(), binding.flMain.id, false)
        binding.bottomNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    supportActionBar?.title = getString(R.string.games_for_you)
                    replaceFragment(HomeFragment(), binding.flMain.id, false)
                }
                R.id.favorites -> {
                    supportActionBar?.title = getString(R.string.favorites)
                    replaceFragment(FavoritesFragment(), binding.flMain.id, false)
                }
            }
            return@setOnItemSelectedListener true
        }
    }
}