package id.syaafiqi.game_directory_app.features.home

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeDatasource {
    @GET("games")
    fun getPhotosList(
        @Query("page") page: Int,
        @Query("page_size") limit: Int,
        @Query("search") keyword: String,
        @Query("key") token: String
    ): Observable<HomeResponse>
}