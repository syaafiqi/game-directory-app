package id.syaafiqi.game_directory_app.features.home

import id.syaafiqi.game_directory_app.BuildConfig
import id.syaafiqi.game_directory_app.core.data.PagedRequest
import id.syaafiqi.game_directory_app.core.domain.DefaultObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeFactory(private val instance: HomeDatasource) {
    fun getGames(pagedRequest: PagedRequest, callback: DefaultObserver<HomeResponse>): Disposable {
        return instance.getPhotosList(pagedRequest.page, pagedRequest.limit, pagedRequest.keyword, BuildConfig.API_KEY)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }
}