package id.syaafiqi.game_directory_app.features.home

import id.syaafiqi.game_directory_app.core.data.PagedRequest
import id.syaafiqi.game_directory_app.core.domain.DefaultObserver
import id.syaafiqi.game_directory_app.services.NetworkProvider
import io.reactivex.disposables.CompositeDisposable

class HomePresenter(
    private val view: HomeView,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) {
    private val factory: HomeFactory =
        HomeFactory(NetworkProvider.retrofitInstance.create(HomeDatasource::class.java))

    fun getGames(paging: PagedRequest) {
        view.onShowLoading()

        val disposable = factory.getGames(
            paging,
            object : DefaultObserver<HomeResponse> {
                override fun onSuccess(entity: HomeResponse) {
                    view.onHideLoading()
                    if (entity.results.isEmpty()) view.isLastPage = true
                    view.onLoadSuccess(entity.results.map { item ->
                        GameModel(
                            id = item.id,
                            name = item.name,
                            rating = item.rating,
                            ratingCount = item.ratingCount,
                            tba = item.tba,
                            releaseDate = item.released,
                            imageUrl = item.imageUrl
                        )
                    })
                }

                override fun onError(exception: Throwable) {
                    view.onHideLoading()
                    view.onLoadFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }

    fun fetchGames(paging: PagedRequest) {
        view.isLoadMore = true

        val disposable = factory.getGames(
            paging,
            object : DefaultObserver<HomeResponse> {
                override fun onSuccess(entity: HomeResponse) {
                    view.isLoadMore = false
                    if (entity.results.isEmpty()) view.isLastPage = true
                    view.onFetchSuccess(entity.results.map { item ->
                        GameModel(
                            id = item.id,
                            name = item.name,
                            rating = item.rating,
                            ratingCount = item.ratingCount,
                            tba = item.tba,
                            releaseDate = item.released,
                            imageUrl = item.imageUrl
                        )
                    })
                }

                override fun onError(exception: Throwable) {
                    view.isLoadMore = false
                    view.onFetchFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }
}