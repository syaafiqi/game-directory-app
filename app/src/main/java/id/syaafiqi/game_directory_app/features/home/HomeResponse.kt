package id.syaafiqi.game_directory_app.features.home

import com.google.gson.annotations.SerializedName

data class HomeResponse(
    val count: Int,
    val results: List<Data>
) {
    data class Data(
        val id: Int,
        val name: String,
        val slug: String,
        val released: String,
        val tba: Boolean,
        @SerializedName("background_image")
        val imageUrl: String,
        val rating: Double,
        @SerializedName("ratings_count")
        val ratingCount: Int
    )
}
