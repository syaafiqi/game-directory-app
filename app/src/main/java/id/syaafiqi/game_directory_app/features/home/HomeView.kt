package id.syaafiqi.game_directory_app.features.home

interface HomeView {
    fun onShowLoading()
    fun onHideLoading()
    fun onFetchSuccess(models: List<GameModel>)
    fun onFetchFailed()
    fun onLoadSuccess(models: List<GameModel>)
    fun onLoadFailed()

    var isLastPage: Boolean
    var isLoadMore: Boolean
}