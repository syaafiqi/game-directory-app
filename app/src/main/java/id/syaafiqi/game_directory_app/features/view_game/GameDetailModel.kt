package id.syaafiqi.game_directory_app.features.view_game

data class GameDetailModel(
    val id: Int,
    val name: String,
    val developer: String,
    val releaseDate: String,
    val rating: Double,
    val playCount: Int,
    val description: String,
    val imageUrl: String
)
