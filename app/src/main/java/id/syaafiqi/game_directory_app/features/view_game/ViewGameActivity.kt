package id.syaafiqi.game_directory_app.features.view_game

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.text.HtmlCompat
import id.syaafiqi.game_directory_app.R
import id.syaafiqi.game_directory_app.core.storage.delete
import id.syaafiqi.game_directory_app.core.storage.find
import id.syaafiqi.game_directory_app.core.storage.insert
import id.syaafiqi.game_directory_app.core.storage.storage
import id.syaafiqi.game_directory_app.core.storage.tables.Favorites
import id.syaafiqi.game_directory_app.core.storage.tables.FavoritesModel
import id.syaafiqi.game_directory_app.databinding.ActivityViewGameBinding
import id.syaafiqi.game_directory_app.extensions.*
import id.syaafiqi.game_directory_app.features.home.GameModel
import id.syaafiqi.game_directory_app.utils.Constants

class ViewGameActivity : AppCompatActivity() {
    private val observer: ViewGameObserver = ViewGameObserver()
    private var presenter: ViewGamePresenter = ViewGamePresenter(observer)
    private lateinit var binding: ActivityViewGameBinding
    private var gameInfo: GameModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gameInfo = getExtra(Constants.GAME_INFO)

        initView()
    }

    private fun initView() {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = gameInfo?.name ?: ""
        }
        presenter.findGame(gameInfo?.id ?: -1)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_view_game, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favorite -> favoriteAction()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val existingFavorite = storage().find(Favorites, gameInfo?.id ?: -1)
        existingFavorite?.let {
            menu?.findItem(R.id.favorite)?.icon =
                AppCompatResources.getDrawable(this, R.drawable.ic_favorite_active_24)
        } ?: run {
            menu?.findItem(R.id.favorite)?.icon =
                AppCompatResources.getDrawable(this@ViewGameActivity, R.drawable.ic_favorite_inactive_24)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressedDispatcher.onBackPressed()
        return true
    }

    private fun favoriteAction() {
        gameInfo?.let { game ->
            val existing = storage().find(Favorites, game.id)
            // If existing exist then remove
            existing?.let {
                storage().delete(
                    Favorites, it.id
                )
                binding.root.showToast(getString(R.string.removed_from_favorites))
                invalidateOptionsMenu()
            } ?: run {
                // If not exist then insert
                storage().insert(
                    Favorites, FavoritesModel(
                        id = game.id,
                        name = game.name,
                        rating = game.rating,
                        ratingCount = game.ratingCount,
                        imageUrl = game.imageUrl,
                        releaseDate = game.releaseDate,
                        tba = game.tba
                    )
                )
                binding.root.showToast(getString(R.string.added_to_favorites))
                invalidateOptionsMenu()
            }
        }
    }

    inner class ViewGameObserver : ViewGameView {
        override fun onShowLoading() {
            binding.loadingState.root.show()
        }

        override fun onHideLoading() {
            binding.loadingState.root.hide()
        }

        override fun onLoadSuccess(model: GameDetailModel) {
            with(binding) {
                ivGameImage.load(model.imageUrl)
                tvGameTitle.text = model.name
                tvGameReleaseDate.text = getString(R.string.release_date_label, model.releaseDate)
                tvGameDeveloper.text = model.developer
                tvGameRating.text = model.rating.toString()
                tvGamePlayed.text = model.playCount.toString()
                tvGameDescription.text =
                    HtmlCompat.fromHtml(model.description, HtmlCompat.FROM_HTML_MODE_LEGACY)
            }

        }

        override fun onLoadFailed() {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }
    }
}