package id.syaafiqi.game_directory_app.features.view_game

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ViewGameDatasource {
    @GET("games/{id}")
    fun getGameDetail(
        @Path("id") id: Int,
        @Query("key") token: String
    ): Observable<ViewGameResponse>
}