package id.syaafiqi.game_directory_app.features.view_game

import id.syaafiqi.game_directory_app.BuildConfig
import id.syaafiqi.game_directory_app.core.domain.DefaultObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ViewGameFactory(private val instance: ViewGameDatasource) {
    fun findGame(id: Int, callback: DefaultObserver<ViewGameResponse>): Disposable {
        return instance.getGameDetail(id, BuildConfig.API_KEY)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }
}