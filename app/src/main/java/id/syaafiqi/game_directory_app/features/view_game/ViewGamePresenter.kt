package id.syaafiqi.game_directory_app.features.view_game

import id.syaafiqi.game_directory_app.core.domain.DefaultObserver
import id.syaafiqi.game_directory_app.services.NetworkProvider
import io.reactivex.disposables.CompositeDisposable

class ViewGamePresenter(
    private val view: ViewGameView,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) {
    private val factory: ViewGameFactory =
        ViewGameFactory(NetworkProvider.retrofitInstance.create(ViewGameDatasource::class.java))

    fun findGame(id: Int) {
        view.onShowLoading()

        val disposable = factory.findGame(
            id,
            object : DefaultObserver<ViewGameResponse> {
                override fun onSuccess(entity: ViewGameResponse) {
                    view.onHideLoading()
                    view.onLoadSuccess(
                        GameDetailModel(
                            id = entity.id,
                            name = entity.name,
                            developer = entity.publishers.joinToString(", ") { item -> item.name },
                            releaseDate = entity.released,
                            rating = entity.rating,
                            playCount = entity.playTime,
                            description = entity.description,
                            imageUrl = entity.backgroundImage
                        )
                    )
                }

                override fun onError(exception: Throwable) {
                    view.onHideLoading()
                    view.onLoadFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }
}
