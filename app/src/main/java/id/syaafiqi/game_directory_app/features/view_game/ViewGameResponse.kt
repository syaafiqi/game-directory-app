package id.syaafiqi.game_directory_app.features.view_game

import com.google.gson.annotations.SerializedName

data class ViewGameResponse(
    val id: Int,
    val name: String,
    val description: String,
    val tba: Boolean,
    val released: String,
    @SerializedName("background_image")
    val backgroundImage: String,
    val rating: Double,
    @SerializedName("playtime")
    val playTime: Int,
    val publishers: List<Publishers>
) {
    data class Publishers(
        val id: Int,
        val name: String,
    )
}
