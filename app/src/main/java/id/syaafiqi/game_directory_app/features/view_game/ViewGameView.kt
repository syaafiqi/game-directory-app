package id.syaafiqi.game_directory_app.features.view_game

interface ViewGameView {
    fun onShowLoading()
    fun onHideLoading()
    fun onLoadSuccess(models: GameDetailModel)
    fun onLoadFailed()
}