package id.syaafiqi.game_directory_app.utils

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import java.util.*

object Constants {
    const val GAME_INFO: String = "GAME_INFO"
    const val POP_BACK_STACK_STATE = "root_fragment"
}